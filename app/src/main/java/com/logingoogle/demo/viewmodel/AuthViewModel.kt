package com.logingoogle.demo.viewmodel

import androidx.lifecycle.ViewModel
import com.logingoogle.demo.entity.User
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

/**
 * Created              : rahman.suwito on 29/11/2022.
 * Date Created         : 29/11/2022 / 06:33 PM
 * ===================================================
 * Package              : com.logingoogle.demo.viewmodel
 * Project Name         : BCAlife
 * Copyright            : Copyright @ 2022 salt.co.id .
 */
class AuthViewModel : ViewModel() {
    private val _user: MutableStateFlow<User?> = MutableStateFlow(null)
    val user: StateFlow<User?> = _user

    suspend fun signIn(email: String, displayName: String) {
        delay(2000) // Simulating network call
        _user.value = User(email, displayName)
    }
}