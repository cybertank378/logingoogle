package com.logingoogle.demo.entity

data class User(
    val email: String,
    val displayName: String
)
