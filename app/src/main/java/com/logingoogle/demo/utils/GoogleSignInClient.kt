package com.logingoogle.demo.utils

import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions

/**
 * Created              : rahman.suwito on 29/11/2022.
 * Date Created         : 29/11/2022 / 06:38 PM
 * ===================================================
 * Package              : com.logingoogle.demo.utils
 * Project Name         : BCAlife
 * Copyright            : Copyright @ 2022 salt.co.id .
 */
fun getGoogleSignInClient(context: Context): GoogleSignInClient {
    val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//         Request id token if you intend to verify google user from your backend server
//        .requestIdToken(context.getString(R.string.backend_client_id))
        .requestEmail()
        .build()

    return GoogleSignIn.getClient(context, signInOptions)
}